﻿using System.Collections.Generic;
using System.Windows.Media.Imaging;

namespace Data
{
    public partial class JsonViewModel : NotifyPropertyChanged
    {
        NameDictionary<NameDictionary<CarModel>> root;
        NameDictionary<CarModel> models;
        CarModel links;
        private string category;
        private string model;
        private readonly string[] EmptyStrings = {};
        private readonly CarModel EmptyCarModel = new CarModel();

        public JsonViewModel()
        {

        }

        public IEnumerable<string> Categories
        {
            get { return root.Keys; }
        }

        public string Category
        {
            get { return category; }

            set
            {
                models = root[value];
                category = value;
                FirePropertyChanged("Models");
            }
        }

        public IEnumerable<string> Models
        {
            get { return null != models ? models.Keys : EmptyStrings; }
        }

        public string Model
        {
            get { return model; }

            set
            {
                Links = models[value];
                model = value;
                FirePropertyChanged("Logo");
            }
        }

        public BitmapImage Logo
        {
            get { return links.Logo; }
            set
            {
                links.Logo = value;
                FirePropertyChanged("Logo");
            }
        }
        

        public CarModel Links
        {
            get { return links ?? EmptyCarModel; }
            set
            {
                links = value;
                FirePropertyChanged("Links");
            }
        }

        public void AddCategory(string p)
        {
            root[p] = new NameDictionary<CarModel>();
            FirePropertyChanged("Categories");
        }

        public void RemoveCategory(string p)
        {
            root.Remove(p);
            FirePropertyChanged("Categories");
        }

        public void AddModel(string p1, string p2)
        {
            root[p1][p2] = new CarModel();
            FirePropertyChanged("Models");
        }

        public void RemoveModel(string p1, string p2)
        {
            root[p1].Remove(p2);
            FirePropertyChanged("Models");
        }

        public void AddLink()
        {
            links[string.Empty] = string.Empty;
        }
    }
}
