﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Media.Imaging;

namespace Data
{
    public class CarModel : NameDictionary<string>, INotifyPropertyChanged
    {
        private byte[] logoData;

        public byte[] LogoData
        {
            get
            {
                if (null == logoData)
                {
                    if (null == logo)
                        return null;
                    logoData = ((MemoryStream) logo.StreamSource).ToArray();
                }
                return logoData;
            }

            set
            {
                logoData = value;
                FirePropertyChanged("logo");
            }
        }
        

        private BitmapImage logo;

        public BitmapImage Logo
        {
            get
            {
                if (null == logo)
                {
                    if (null == logoData)
                        return null;
                    logo = new BitmapImage();
                    logo.BeginInit();
                    logo.StreamSource = new MemoryStream(logoData);
                    logo.EndInit();
                    logoData = null;
                }
                return logo;
            }

            set
            {
                logo = value;
                FirePropertyChanged("logo");
            }
        }

        public void FirePropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
