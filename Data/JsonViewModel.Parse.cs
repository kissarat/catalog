﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using Newtonsoft.Json.Linq;

namespace Data
{
    public partial class JsonViewModel
    {
        public JObject Create()
        {
            var jobject = new JObject();
            jobject["catalog"] = new JObject();
            return jobject;
        }

        public void Load(string filename)
        {
            JObject json = File.Exists(filename)
                ? JObject.Parse(File.ReadAllText(filename))
                : Create();
            json = (JObject)json["catalog"];
            root = new NameDictionary<NameDictionary<CarModel>>();
            foreach (var category in json)
            {
                var models = new NameDictionary<CarModel>();
                foreach (var model in (JObject)category.Value)
                {
                    var links = new CarModel();
                    var carModel = (JObject)model.Value;
                    var logoToken = carModel["logo"];
                    if (JTokenType.Null != logoToken.Type)
                        links.LogoData = (byte[])logoToken;
                    foreach (var link in (JObject)carModel["links"])
                    {
                        links[link.Key] = (string)link.Value;
                    }
                    models[model.Key] = links;
                }
                root[category.Key] = models;
            }
            FirePropertyChanged("Categories");
        }

        public void Save(string filename)
        {
            var jsonCatalog = new JObject();
            foreach (var category in root)
            {
                var models = new JObject();
                foreach (var model in category.Value)
                {
                    var carModel = new JObject();
                    var links = new JObject();
                    foreach (var link in model.Value)
                    {
                        links[link.Name] = link.Value;
                    }
                    carModel["logo"] = model.Value.LogoData;
                    carModel["links"] = links;
                    models[model.Name] = carModel;
                }
                jsonCatalog[category.Name] = models;
            }
            var json = new JObject();
            json["catalog"] = jsonCatalog;
            File.WriteAllText(filename, json.ToString());
        }

        public byte[] Compress(byte[] data, CompressionMode mode)
        {
            var stream = new MemoryStream();
            var compression = new GZipStream(stream, mode);
            if (CompressionMode.Compress == mode)
                compression.Write(data, 0, data.Length);
            else
                compression.Read(data, 0, data.Length);
            return stream.ToArray();
        }
    }
}
