﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net.Sockets;

namespace Data
{
    public class NameDictionary<T> : List<NamedValueNotify<T>>, INotifyCollectionChanged
        where T : class
    {
        public NameDictionary()
        {
            
        }

        public NameDictionary(IEnumerable<KeyValuePair<string, T>> source)
        {
            foreach (var kv in source)
            {
                this[kv.Key] = kv.Value;
            }
        }

        public T this[string name]
        {
            get
            { return (from namedValue in this where name == namedValue.Name select namedValue.Value).FirstOrDefault(); }

            set
            {
                var nv = (from _ in this
                    where name == _.Name
                    select _).FirstOrDefault();
                if (null == nv)
                {
                    nv = new NamedValueNotify<T>();
                    Add(nv);
                    FireCollectionChanged();
                }
                nv.Name = name;
                nv.Value = value;
            }
        }

        public void To<TMap>(TMap dest)
            where TMap : IDictionary<string, T>
        {
            foreach (var kv in this)
            {
                dest[kv.Name] = kv.Value;
            }
        }

        public IEnumerable<string> Keys
        {
            get { return new NamedEnumerable<T>(this); }
        }

        public void Remove(string name)
        {
            Remove((from _ in this
                where name == _.Name
                select _).First());
            FireCollectionChanged();
        }

        public new void RemoveAt(int index)
        {
            base.RemoveAt(index);
            FireCollectionChanged();
        }

        public void FireCollectionChanged()
        {
            if (null != CollectionChanged)
                CollectionChanged(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        public event NotifyCollectionChangedEventHandler CollectionChanged;
    }

    public class NamedEnumerable<T> : IEnumerable<string>
        where T : class 
    {
        private NameDictionary<T> map; 

        public NamedEnumerable(NameDictionary<T> map)
        {
            this.map = map;
        }

        public IEnumerator<string> GetEnumerator()
        {
            return new NamedEnumerator<T>(map.GetEnumerator());
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return new NamedEnumerator<T>(map.GetEnumerator());
        }
    }

    public class NamedEnumerator<T> : IEnumerator<string>
        where T : class 
    {
        private readonly IEnumerator<NamedValueNotify<T>> iterator;
        public NamedEnumerator(IEnumerator<NamedValueNotify<T>> iterator)
        {
            this.iterator = iterator;
        }

        public string Current
        {
            get { return iterator.Current.Name; }
        }

        public void Dispose()
        {
            iterator.Dispose();
        }

        object IEnumerator.Current
        {
            get { return iterator.Current.Name; }
        }

        public bool MoveNext()
        {
            return iterator.MoveNext();
        }

        public void Reset()
        {
            iterator.Reset();
        }
    }
}
