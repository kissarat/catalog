﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Data
{
    public class NamedValueNotify<T> : NotifyPropertyChanged
    {
        private string name;

        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                FirePropertyChanged("Key");
            }
        }

        private T value;

        public T Value
        {
            get { return value; }
            set { this.value = value; }
        }
        
    }
}
