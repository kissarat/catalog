﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Reflection;
using System.Runtime.InteropServices;
using System.ComponentModel;
using Microsoft.Win32;
using SHDocVw;
using System.Diagnostics;
using System.Text;
using System.Threading;
using System.Net;
using Data;

namespace Crutch
{
    static class Program
    {
        //[STAThread]
        static void Main()
        {
            var vm = new ViewModel();
            return;
            var tray = new NotifyIcon();
            tray.Icon = new Icon("car.ico");
            tray.ContextMenu = new ContextMenu(new [] {
                new MenuItem("Закрыть",  delegate(object sender, EventArgs e) {
                    foreach (var process in Process.GetProcessesByName("Shell"))
                        process.Kill();
                    Environment.Exit(0);
                }),
                new MenuItem("Открыть", Open)
            });
            tray.Text = "АвтоПоиск";
            foreach (var process in Process.GetProcessesByName("iexplore"))
            {
                process.Kill();
                process.WaitForExit();
            }
            tray.Visible = true;
            new Thread(Serve).Start();
            Open();
            Application.Run();
        }

        static void Serve()
        {
            var server = new HttpListener();
            server.Prefixes.Add("http://localhost:13666/");
            server.Start();
            while (true)
            {
                var context = server.GetContext();
                var url = context.Request.Headers["X-Open"];
                if (null != url && url.Length > 3)
                {
                    if (url.StartsWith("http://"))
                        Go(url);
                    else
                        Process.Start(url);
                }   
                else
                {
                    context.Response.StatusCode = 302;
                    context.Response.Headers["Location"] = "http://www.cnews.ru/top/2013/05/13/razrabotchik_microsoft_obyasnil_pochemu_linux_luchshe_chem_windows_528539";
                }
                context.Response.Close();
            }
        }

        static InternetExplorer Go(string url)
        {
            //RegistryKey registry = Registry.CurrentUser.OpenSubKey(
            //    @"Software\Microsoft\Windows\CurrentVersion\Internet Settings", true);
            //registry.SetValue("ProxyEnable", 1);
            //registry.SetValue("ProxyServer", "192.168.0.4:8118");
            var ie = new InternetExplorer();
            ie.AddressBar = false;
            ie.ToolBar = 0;
            ie.StatusBar = false;
            ie.MenuBar = false;
            ie.Navigate(url);
            ie.Visible = true;
            //registry.SetValue("ProxyEnable", 0);
            //registry.Close();
            return ie;
        }

        static void Open(object sender = null, EventArgs e = null)
        {
                var chromeApp = new Process();
                chromeApp.StartInfo.FileName = @"C:\opt\Chrome\Shell.exe";
                chromeApp.StartInfo.Arguments = "--app-id=jhhkdahlolaldfbpoijjnfdkgepgafmn";
                chromeApp.Start();
        }
    }
}
