﻿using Data;
using Microsoft.Win32;
using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace Admin
{
    public partial class MainWindow : Window
    {
        private readonly string AppData;
        private readonly string Catalog;
        private const int imageSize = 64;

        public JsonViewModel ViewModel
        {
            get { return DataContext as JsonViewModel; }
            set { DataContext = value; }
        }

        private OpenFileDialog fileDialog;

        public OpenFileDialog FileDialog
        {
            get
            {
                if (null == fileDialog)
                {
                    fileDialog = new OpenFileDialog();
                    fileDialog.Filter = "Image Files (*.jpg, *.png, *.gif, *.bmp)|*.jpg;*.png;*.gif;*.bmp";
                }
                return fileDialog;
            }

        }

        public MainWindow()
        {
            InitializeComponent();
            ViewModel = new JsonViewModel();
            AppData = Path.Combine(
                Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "autosearch");
            Catalog = Path.Combine(AppData, "catalog.json");
            if (!File.Exists(AppData))
            {
                Directory.CreateDirectory(AppData);
            }
        }

        private void addCategory_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.AddCategory(category.Text);
        }

        private void removeCategory_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.RemoveCategory(category.Text);
        }

        private void addModel_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.AddModel(category.Text, model.Text);
        }

        private void removeModel_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.RemoveModel(category.Text, model.Text);
        }

        private void add_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.AddLink();
        }

        private void model_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //var data = ViewModel.Links;
            //if (null != data)
            //    links.ItemsSource = data;
        }

        private void MainWindow_OnLoaded(object sender, RoutedEventArgs e)
        {
            ViewModel.Load(Catalog);
        }

        private void MainWindow_OnClosed(object sender, EventArgs e)
        {
            ViewModel.Save(Catalog);
        }

        private void Remove_OnClick(object sender, RoutedEventArgs e)
        {
            ViewModel.Links.RemoveAt(links.SelectedIndex);
        }

        private void Logo_OnClick(object sender, RoutedEventArgs e)
        {
            if (true == FileDialog.ShowDialog())
            {
                try
                {
                    var data = File.ReadAllBytes(fileDialog.FileName);
                    var stream = new MemoryStream(data);
                    var bitmap = new BitmapImage();
                    bitmap.BeginInit();
                    bitmap.StreamSource = stream;
                    bitmap.CacheOption = BitmapCacheOption.OnLoad;
                    if (bitmap.DecodePixelWidth > bitmap.DecodePixelHeight)
                        bitmap.DecodePixelWidth = imageSize;
                    else
                        bitmap.DecodePixelHeight = imageSize;
                    bitmap.EndInit();
                    stream = new MemoryStream();
                    var jpeg = new JpegBitmapEncoder();
                    jpeg.Frames.Add(BitmapFrame.Create(bitmap));
                    jpeg.QualityLevel = 75;
                    jpeg.Save(stream);
                    stream.Seek(0, SeekOrigin.Begin);
                    bitmap = new BitmapImage();
                    bitmap.BeginInit();
                    bitmap.StreamSource = stream;
                    bitmap.CacheOption = BitmapCacheOption.OnLoad;
                    bitmap.EndInit();
                    ViewModel.Logo = bitmap;
                    stream.Close();
                }
                catch (NotSupportedException ex)
                {
                    MessageBox.Show(this, "Формат файла не поддерживается");
                }
            }
        }
    }
}
