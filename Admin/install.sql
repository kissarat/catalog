﻿create table Category (
ID integer primary key autoincrement,
Name text
);

create table Model (
ID integer primary key autoincrement,
CategoryID int,
Logo blob,
Name text,
foreign key (CategoryID) references Category(ID)
);

create table Link (
ModelID int,
Name text,
Path text,
foreign key (ModelID) references Model(ID)
);